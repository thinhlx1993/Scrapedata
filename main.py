import os
import time
import requests
import multiprocessing
from lxml import html
from peewee import *
from multiprocessing import Process, Queue

db = SqliteDatabase('datascraper.db')
list_url_queue = Queue()
list_data_queue = Queue()


class ListingPage(Model):
    job_id = CharField()
    job_title = CharField()
    posted_date = CharField()
    age = CharField()
    requirements = CharField()
    salary = CharField()
    period = CharField()
    working_hour = CharField()
    location = CharField()

    class Meta:
        database = db


def info(title):
    print(title)
    print('module name:', __name__)
    if hasattr(os, 'get process id'):
        print('parent process:', os.getppid())
    print('process id:', os.getpid())
    print("==========================================================")


def save_list_data():
    while list_data_queue:
        params = list_data_queue.get()
        if params != "Done":
            save_data = ListingPage(job_id=params[0], job_title=params[1],
                                    posted_date=params[2], age=params[3], requirements=params[4],
                                    salary_range=params[5], period=params[6], working_hour=params[7],
                                    location=params[8])
            save_data.save()
        else:
            print("Done===========================================================================")
            break


def check_page(url):
    if url != "Done":
        page_load = requests.get(url)
        tree_load = html.fromstring(page_load.content)
        job_id = tree_load.xpath('//h4[@class="title"]/span/text()')
        job_title = tree_load.xpath('//h4[@class="title"]/a/text()')

        for i in range(0, len(job_id)):
            # print("job_id: ", job_id[i])
            # print("job_title: ", job_title[i])
            posted_date = tree_load.xpath('//*[@id="main"]/div[' + str(i + 2) + ']/p[1]/span[2]/text()')
            if posted_date:
                posted_string = posted_date[-1]
            else:
                posted_string = "null"
            # print("posted_date: ", posted_date)

            age = tree_load.xpath('//*[@id="main"]/div[' + str(i + 2) + ']/table/tr[1]/td/text()')
            if age:
                age_string = age[-1]
            else:
                age_string = "null"
            # print("age: ", age)

            requirements = tree_load.xpath('//*[@id="main"]/div[' + str(i + 2) + ']/table/tr[2]/td/text()')
            if requirements:
                requirements_string = requirements[-1]
            else:
                requirements_string = "null"
            # print("requirements: ", requirements)

            salary = tree_load.xpath('//*[@id="main"]/div[' + str(i + 2) + ']/table/tr[3]/td/text()')
            if salary:
                salary_string = salary[-1]
            else:
                salary_string = "null";
            # print("salary: ", salary)

            period = tree_load.xpath('//*[@id="main"]/div[' + str(i + 2) + ']/table/tr[4]/td/text()')
            if period:
                period_string = period[-1]
            else:
                period_string = "null";
            # print("period: ", period)

            working_hour = tree_load.xpath('//*[@id="main"]/div[' + str(i + 2) + ']/table/tr[5]/td/text()')
            if working_hour:
                working_hour_string = working_hour[-1]
            else:
                working_hour_string = "null";
            #print("working_hour: ", working_hour[-1])

            location = tree_load.xpath('//*[@id="main"]/div[' + str(i + 2) + ']/table/tr[6]/td/text()')
            if location:
                location_string = location[-1]
            else:
                location_string = "null";
            # print("location: ", location)
            # print("==============================================================")

            list_data_queue.put([str(job_id[i]), str(job_title[i]), posted_string, age_string,
                                 str(requirements_string), str(salary_string), str(period_string),
                                 str(working_hour_string), str(location_string)]) 

    else:
        list_data_queue.put("Done")


def extract_url(url):
    info('Extract Url Function')

    # 1st page
    page = requests.get(url)
    tree = html.fromstring(page.content)

    # get last page
    page_count = tree.xpath('//span[@class="pagenavi-box-a"]/a/@href')

    page_id = page_count[-1].split('/')  # id page last

    # queue url
    for i in range(1,int(page_id[-2])):
        url_page = url + "page/" + str(i)
        list_url_queue.put(url_page)
    list_url_queue.put("Done")
    # create worker do work
    for i in range(1, 5):
        process_p = Process(target=processing_list_url_queue, args=(list_url_queue,))
        process_p.start()
    # process insert data
    process_insert_data = Process(target=save_list_data)
    process_insert_data.start()


def processing_list_url_queue(queue_url):
    while queue_url:
        name = multiprocessing.current_process().name
        print(name, 'Starting')
        items = queue_url.get()
        if items != "Done":
            check_page(items)
            print(name, 'Exiting')
        else:
            print("Done")
            break


if __name__ == '__main__':
    _start = time.time()  # start timer counter
    url_main = 'http://4510m.in/'
    extract_url = Process(target=extract_url, args=(url_main,))
    extract_url.start()
    extract_url.join()
    print("Timer counter %s seconds" % (time.time() - _start))
